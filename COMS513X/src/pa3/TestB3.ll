; ModuleID = 'TestB3.c'
source_filename = "TestB3.c"
target datalayout = "e-m:o-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-apple-macosx10.13.0"

@.str = private unnamed_addr constant [9 x i8] c"x = %d; \00", align 1
@.str.1 = private unnamed_addr constant [9 x i8] c"y = %d; \00", align 1
@.str.2 = private unnamed_addr constant [9 x i8] c"z = %d; \00", align 1

; Function Attrs: noinline nounwind optnone ssp uwtable
define i32 @main() #0 {
entry:
  %retval = alloca i32, align 4
  %a = alloca i32, align 4
  %b = alloca i32, align 4
  %c = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %x = alloca i32, align 4
  %y = alloca i32, align 4
  %z = alloca i32, align 4
  store i32 0, i32* %retval, align 4
  store i32 5, i32* %a, align 4
  store i32 3, i32* %b, align 4
  store i32 8, i32* %c, align 4
  %0 = load i32, i32* %a, align 4
  %1 = load i32, i32* %b, align 4
  %add = add nsw i32 %0, %1
  store i32 %add, i32* %x, align 4
  %2 = load i32, i32* %b, align 4
  %3 = load i32, i32* %c, align 4
  %add1 = add nsw i32 %2, %3
  store i32 %add1, i32* %y, align 4
  store i32 0, i32* %z, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc21, %entry
  %4 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %4, 10
  br i1 %cmp, label %for.body, label %for.end23

for.body:                                         ; preds = %for.cond
  store i32 5, i32* %z, align 4
  %5 = load i32, i32* %i, align 4
  switch i32 %5, label %sw.epilog [
    i32 5, label %sw.bb
    i32 3, label %sw.bb6
    i32 8, label %sw.bb7
  ]

sw.bb:                                            ; preds = %for.body
  store i32 0, i32* %j, align 4
  br label %for.cond2

for.cond2:                                        ; preds = %for.inc, %sw.bb
  %6 = load i32, i32* %j, align 4
  %cmp3 = icmp slt i32 %6, 10
  br i1 %cmp3, label %for.body4, label %for.end

for.body4:                                        ; preds = %for.cond2
  %7 = load i32, i32* %j, align 4
  %rem = srem i32 %7, 2
  %cmp5 = icmp eq i32 %rem, 0
  br i1 %cmp5, label %if.then, label %if.end

if.then:                                          ; preds = %for.body4
  %8 = load i32, i32* %a, align 4
  store i32 %8, i32* %x, align 4
  %9 = load i32, i32* %b, align 4
  %10 = load i32, i32* %z, align 4
  %mul = mul nsw i32 %9, %10
  store i32 %mul, i32* %x, align 4
  %11 = load i32, i32* %x, align 4
  store i32 %11, i32* %y, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body4
  %12 = load i32, i32* %i, align 4
  store i32 %12, i32* %z, align 4
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %13 = load i32, i32* %j, align 4
  %inc = add nsw i32 %13, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond2

for.end:                                          ; preds = %for.cond2
  br label %sw.epilog

sw.bb6:                                           ; preds = %for.body
  %14 = load i32, i32* %y, align 4
  store i32 %14, i32* %x, align 4
  %15 = load i32, i32* %j, align 4
  store i32 %15, i32* %x, align 4
  br label %sw.epilog

sw.bb7:                                           ; preds = %for.body
  %16 = load i32, i32* %b, align 4
  %17 = load i32, i32* %i, align 4
  %mul8 = mul nsw i32 %16, %17
  %rem9 = srem i32 %mul8, 20
  %cmp10 = icmp eq i32 %rem9, 3
  br i1 %cmp10, label %if.then11, label %if.else

if.then11:                                        ; preds = %sw.bb7
  store i32 12, i32* %x, align 4
  br label %if.end18

if.else:                                          ; preds = %sw.bb7
  %18 = load i32, i32* %a, align 4
  %19 = load i32, i32* %i, align 4
  %mul12 = mul nsw i32 %18, %19
  %rem13 = srem i32 %mul12, 13
  %cmp14 = icmp eq i32 %rem13, 7
  br i1 %cmp14, label %if.then15, label %if.else16

if.then15:                                        ; preds = %if.else
  %20 = load i32, i32* %x, align 4
  store i32 %20, i32* %z, align 4
  br label %if.end17

if.else16:                                        ; preds = %if.else
  store i32 5, i32* %y, align 4
  br label %if.end17

if.end17:                                         ; preds = %if.else16, %if.then15
  br label %if.end18

if.end18:                                         ; preds = %if.end17, %if.then11
  br label %sw.epilog

sw.epilog:                                        ; preds = %if.end18, %for.body, %sw.bb6, %for.end
  %21 = load i32, i32* %b, align 4
  %22 = load i32, i32* %i, align 4
  %add19 = add nsw i32 %21, %22
  store i32 %add19, i32* %x, align 4
  %23 = load i32, i32* %a, align 4
  %24 = load i32, i32* %b, align 4
  %add20 = add nsw i32 %23, %24
  store i32 %add20, i32* %x, align 4
  %25 = load i32, i32* %x, align 4
  store i32 %25, i32* %y, align 4
  br label %for.inc21

for.inc21:                                        ; preds = %sw.epilog
  %26 = load i32, i32* %i, align 4
  %inc22 = add nsw i32 %26, 1
  store i32 %inc22, i32* %i, align 4
  br label %for.cond

for.end23:                                        ; preds = %for.cond
  %27 = load i32, i32* %x, align 4
  %call = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str, i32 0, i32 0), i32 %27)
  %28 = load i32, i32* %y, align 4
  %call24 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.1, i32 0, i32 0), i32 %28)
  %29 = load i32, i32* %z, align 4
  %call25 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.2, i32 0, i32 0), i32 %29)
  ret i32 0
}

declare i32 @printf(i8*, ...) #1

attributes #0 = { noinline nounwind optnone ssp uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="penryn" "target-features"="+cx16,+fxsr,+mmx,+sse,+sse2,+sse3,+sse4.1,+ssse3,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="penryn" "target-features"="+cx16,+fxsr,+mmx,+sse,+sse2,+sse3,+sse4.1,+ssse3,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{!"clang version 7.0.0 (trunk 324313)"}
