import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.EdgeFactory;
import java.util.HashMap;
import java.io.FileReader;
import java.util.Scanner;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.lang.Integer;
import java.util.Set;
import java.util.ListIterator;
import java.io.IOException;
import java.util.LinkedList;
import java.util.HashSet;

class DataFlow
{
    public static void main(String[] args)
    {
        class VectorFactory implements EdgeFactory<Integer, String>
        {
          public String createEdge(Integer sourceVertex, Integer targetVertex)
          {
            String edge = sourceVertex.toString() + '-' + targetVertex.toString();
            return edge;
          }
        }
      
      //Variables
      String fileName;
      DefaultDirectedGraph<Integer, String> digraph = new DefaultDirectedGraph<Integer, String>(new VectorFactory());
      HashMap<String, Integer> nodes = new HashMap<String, Integer>();
      Scanner input = new Scanner(System.in);
      int graphSize = 0;
      int defNum = 1;
      ArrayList<String> labels = new ArrayList<String>();
      LinkedList<Integer> changeList = new LinkedList<Integer>();
      ArrayList<String> blocks = new ArrayList<String>();
      ArrayList<String> definitions = new ArrayList<String>();
      ArrayList<HashSet<String>> out;
      ArrayList<HashSet<String>> in;
      ArrayList<LinkedList<String>> gen = new ArrayList<LinkedList<String>>();
      ArrayList<HashSet<String>> kill = new ArrayList<HashSet<String>>();
      ArrayList<HashSet<Integer>> pred = new ArrayList<HashSet<Integer>>();
      ArrayList<HashSet<Integer>> succ = new ArrayList<HashSet<Integer>>();

      try
      {
        System.out.println("Enter DOT file: ");
        fileName = input.nextLine();
        FileReader fileVertex = new FileReader(fileName);
        Scanner scanVertex = new Scanner(fileVertex);

        //Read vertexes and the variables in the node.
        while(scanVertex.hasNext())
        {
          String line = scanVertex.nextLine();

          //for.inc is added to break the loops and they don't contain helpful information
          if(line.contains("shape") && !line.contains("{for.inc"))
          {
            int blockStart = line.indexOf('{');
            int blockEnd = line.indexOf('}');
            String block = line.substring(blockStart, blockEnd);
            int nodeStart = line.indexOf('N');
            int nodeEnd = line.indexOf('[') - 1;
            String node = line.substring(nodeStart, nodeEnd);
            int labelStart = line.indexOf('{') + 1;
            int labelEnd = line.indexOf(':');
            String label = line.substring(labelStart, labelEnd);
            labels.add(label);

            digraph.addVertex(graphSize);
            nodes.put(node, new Integer(graphSize));
            blocks.add(block);
            gen.add(new LinkedList<String>());
            kill.add(new HashSet<String>());
            pred.add(new HashSet<Integer>());
            succ.add(new HashSet<Integer>());
            changeList.add(graphSize);
            graphSize++;
          }
        }

        //Read edges
        scanVertex.close();
        fileVertex.close();
        
        FileReader fileEdge = new FileReader(fileName);
        Scanner scanEdge = new Scanner(fileEdge);
        
        while(scanEdge.hasNext())
        {
          String line = scanEdge.nextLine();

          if(line.contains("->"))
          {
            int tailNodeStart = line.indexOf('N');
            int tailNodeEnd;
            if(line.contains(":")) 
            {
              tailNodeEnd = line.indexOf(':');
            } else
            {
              tailNodeEnd = line.indexOf('-') - 1;
            }
            String tailNode = line.substring(tailNodeStart, tailNodeEnd);
            int headNodeStart = line.lastIndexOf('N');
            int headNodeEnd = line.indexOf(';');
            String headNode = line.substring(headNodeStart, headNodeEnd);
            if(nodes.containsKey(headNode) && nodes.containsKey(tailNode))
            {
              Integer tailNodeLabel = new Integer(nodes.get(tailNode));
              Integer headNodeLabel = new Integer(nodes.get(headNode));
              String edge = tailNodeLabel.toString() + '-' + headNodeLabel.toString();

              digraph.addEdge(tailNodeLabel, headNodeLabel, edge);
            }
          }
        }
      scanEdge.close();
      fileEdge.close();

      //Create pred and succ

      for(int i = 0; i < graphSize; i++)
      {
        //pred
        LinkedList<Integer> predQueue = new LinkedList<Integer>();
        Set<String> predIn = digraph.incomingEdgesOf(i);
        for(String inEdge: predIn)
        {
          predQueue.add(digraph.getEdgeSource(inEdge));
        }

        while(predQueue.size() > 0)
        {
          Integer head = predQueue.remove();
          predIn = digraph.incomingEdgesOf(head);
          pred.get(i).add(head);
          for(String inEdge: predIn)
          {
            predQueue.add(digraph.getEdgeSource(inEdge));
          }
        }

        //succ
        LinkedList<Integer> succQueue = new LinkedList<Integer>();
        Set<String> succIn = digraph.outgoingEdgesOf(i);
        for(String outEdge: succIn)
        {
          predQueue.add(digraph.getEdgeTarget(outEdge));
        }

        while(succQueue.size() > 0)
        {
          Integer tail = succQueue.remove();
          succIn = digraph.outgoingEdgesOf(tail);
          succ.get(i).add(tail);
          for(String outEdge: succIn)
          {
            succQueue.add(digraph.getEdgeTarget(outEdge));
          }
        }
      }

      //Create gen and kill

      out = new ArrayList<HashSet<String>>(graphSize);
      in = new ArrayList<HashSet<String>>(graphSize);

      for(int i = 0; i < graphSize; i++)
      {
        out.add(new HashSet<String>());
        in.add(new HashSet<String>());
        String[] statements = blocks.get(i).split("\\\\l");
        for(String stat: statements)
        {
          if(stat.contains("store"))
          {
            int varStart = stat.indexOf("* %")+3;
            int varEnd = stat.indexOf(',', varStart);
            String var = stat.substring(varStart, varEnd);
            gen.get(i).add(var+'#'+defNum);
            definitions.add(var+'#'+defNum);
            defNum++;
          }
        }
      }
      
      for(int i = 0; i < graphSize; i++)
      {
        for(int j = 0; j < definitions.size(); j++)
        {
          String definition = definitions.get(j);
          int defIndex = definition.indexOf('#');
          ListIterator<String> genIter = gen.get(i).listIterator(0);
          while(genIter.hasNext())
          {
            String var = genIter.next();
            int varIndex = var.indexOf('#');
            String variable = var.substring(0, varIndex);
            int varNum = Integer.parseInt(var.substring(varIndex+1));
            String defVar = definition.substring(0, defIndex);
            int defNumber = Integer.parseInt(definition.substring(defIndex+1));
            if(defVar.equals(variable))
            {
              kill.get(i).add(definition);
              if(varNum < defNumber) kill.get(i).remove(definition);
            }
          }
        }
      }

      //Calculate reaching defintion of each block
      while(changeList.size() > 0)
      {
        boolean changed = false;
        Integer n = changeList.remove();
        ArrayList<String> oldOutN = new ArrayList<String>();

        //In for block n
        for(Integer p: pred.get(n))
        {
          for(String pOut: out.get(p))
          {
            in.get(n).add(pOut);
          }
        }

        for(String oldOut: out.get(n))
        {
          oldOutN.add(oldOut);
        }

        //Out for block n
        for(String genN: gen.get(n))
        {
          out.get(n).add(genN);
        }

        for(String inN: in.get(n))
        {
          if(!kill.get(n).contains(inN)) out.get(n).add(inN);
        }

        for(String outN: out.get(n))
        {
          if(!oldOutN.contains(outN)) changed = true;
        }

        if(changed)
        {
          for(Integer succN: succ.get(n))
          {
            if(!changeList.contains(succN)) changeList.add(succN);
          }
        }

      }

      //Print in and out for each block
      for(int i = 0; i < graphSize; i++)
      {
        System.out.println("block " + labels.get(i));
        System.out.print("In: ");
        for(String inDef: in.get(i))
        {
          if(!inDef.contains("retval"))
          {
            System.out.print(inDef + " ");
          }
        }
        System.out.println();
        System.out.print("Gen: ");
        for(String genDef: gen.get(i)) 
        {
          if(!genDef.contains("retval"))
          {
            System.out.print(genDef + " ");
          }
        }
        System.out.println();
        System.out.print("Kill: ");
        for(String killDef: kill.get(i)) 
        {
          if(!killDef.contains("retval"))
          {
            System.out.print(killDef + " ");
          }
        }
        System.out.println();
        System.out.print("Out: ");
        for(String outDef: out.get(i)) 
        {
          if(!outDef.contains("retval"))
          {
            System.out.print(outDef + " ");
          }
        }
        System.out.println();
        System.out.println();
      }


    }catch(FileNotFoundException e)
    {
      System.out.println("File Not Found");
    }catch(IOException e)
    {
      System.out.println("Closing Error");
    }
  }
}
