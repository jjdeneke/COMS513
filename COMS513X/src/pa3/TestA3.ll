; ModuleID = 'TestA3.c'
source_filename = "TestA3.c"
target datalayout = "e-m:o-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-apple-macosx10.13.0"

@.str = private unnamed_addr constant [7 x i8] c"x = %d\00", align 1
@.str.1 = private unnamed_addr constant [9 x i8] c"; y = %d\00", align 1

; Function Attrs: noinline nounwind optnone ssp uwtable
define i32 @main() #0 {
entry:
  %retval = alloca i32, align 4
  %n = alloca i32, align 4
  %i = alloca i32, align 4
  %x = alloca i32, align 4
  %y = alloca i32, align 4
  store i32 0, i32* %retval, align 4
  store i32 0, i32* %x, align 4
  store i32 1, i32* %y, align 4
  store i32 2, i32* %y, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc16, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 10
  br i1 %cmp, label %for.body, label %for.end18

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %n, align 4
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc, %for.body
  %1 = load i32, i32* %n, align 4
  %cmp2 = icmp slt i32 %1, 10
  br i1 %cmp2, label %for.body3, label %for.end

for.body3:                                        ; preds = %for.cond1
  %2 = load i32, i32* %i, align 4
  %3 = load i32, i32* %n, align 4
  %mul = mul nsw i32 %2, %3
  %rem = srem i32 %mul, 3
  %cmp4 = icmp eq i32 %rem, 0
  br i1 %cmp4, label %if.then, label %if.else

if.then:                                          ; preds = %for.body3
  %4 = load i32, i32* %i, align 4
  store i32 %4, i32* %x, align 4
  %5 = load i32, i32* %x, align 4
  %6 = load i32, i32* %i, align 4
  %call = call i32 @addition(i32 %5, i32 %6)
  store i32 %call, i32* %y, align 4
  br label %if.end9

if.else:                                          ; preds = %for.body3
  %7 = load i32, i32* %i, align 4
  %8 = load i32, i32* %n, align 4
  %mul5 = mul nsw i32 %7, %8
  %cmp6 = icmp eq i32 %mul5, 20
  br i1 %cmp6, label %if.then7, label %if.else8

if.then7:                                         ; preds = %if.else
  store i32 20, i32* %y, align 4
  br label %if.end

if.else8:                                         ; preds = %if.else
  %9 = load i32, i32* %y, align 4
  store i32 %9, i32* %x, align 4
  store i32 0, i32* %y, align 4
  br label %if.end

if.end:                                           ; preds = %if.else8, %if.then7
  br label %if.end9

if.end9:                                          ; preds = %if.end, %if.then
  %10 = load i32, i32* %n, align 4
  %11 = load i32, i32* %x, align 4
  %call10 = call i32 @addition(i32 %10, i32 %11)
  store i32 %call10, i32* %x, align 4
  br label %for.inc

for.inc:                                          ; preds = %if.end9
  %12 = load i32, i32* %n, align 4
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %n, align 4
  br label %for.cond1

for.end:                                          ; preds = %for.cond1
  %13 = load i32, i32* %i, align 4
  %rem11 = srem i32 %13, 7
  %cmp12 = icmp eq i32 %rem11, 0
  br i1 %cmp12, label %if.then13, label %if.end15

if.then13:                                        ; preds = %for.end
  %14 = load i32, i32* %i, align 4
  %15 = load i32, i32* %i, align 4
  %mul14 = mul nsw i32 %14, %15
  store i32 %mul14, i32* %x, align 4
  %16 = load i32, i32* %x, align 4
  %17 = load i32, i32* %i, align 4
  %add = add nsw i32 %16, %17
  store i32 %add, i32* %y, align 4
  store i32 3, i32* %x, align 4
  br label %if.end15

if.end15:                                         ; preds = %if.then13, %for.end
  br label %for.inc16

for.inc16:                                        ; preds = %if.end15
  %18 = load i32, i32* %i, align 4
  %inc17 = add nsw i32 %18, 1
  store i32 %inc17, i32* %i, align 4
  br label %for.cond

for.end18:                                        ; preds = %for.cond
  %19 = load i32, i32* %x, align 4
  %call19 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str, i32 0, i32 0), i32 %19)
  %20 = load i32, i32* %y, align 4
  %call20 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.1, i32 0, i32 0), i32 %20)
  ret i32 0
}

; Function Attrs: noinline nounwind optnone ssp uwtable
define i32 @addition(i32 %a, i32 %b) #0 {
entry:
  %a.addr = alloca i32, align 4
  %b.addr = alloca i32, align 4
  store i32 %a, i32* %a.addr, align 4
  store i32 %b, i32* %b.addr, align 4
  %0 = load i32, i32* %a.addr, align 4
  %1 = load i32, i32* %b.addr, align 4
  %add = add nsw i32 %0, %1
  ret i32 %add
}

declare i32 @printf(i8*, ...) #1

attributes #0 = { noinline nounwind optnone ssp uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="penryn" "target-features"="+cx16,+fxsr,+mmx,+sse,+sse2,+sse3,+sse4.1,+ssse3,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="penryn" "target-features"="+cx16,+fxsr,+mmx,+sse,+sse2,+sse3,+sse4.1,+ssse3,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0, !1}
!llvm.ident = !{!2}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{!"clang version 7.0.0 (trunk 324313)"}
