The DataFlow program requires COMS513/COMSX513/src be added to the classpath variable when run so the package in org is picked up.

The source code uses a dot file in the format outputted by llvm from the analysis of a c program file as input and outputs the in, generate, kill, and out for each block of the cfg except for-increment blocks, which are removed to break the for-loops and prevent the code from not converging. The source code does not analyze within a block to prevent the case where a variable is written to twice, e.g.
y = 0;
x = 1;
y = 5;
These will all be picked up by the generate set for the block even though the second assignment to y kills the first one, but because it is all within the same block all are considered. This could be remedied by analyzing generate for multiple assignments to the same variable. The source code uses the traditional reaching definition algorithm with In[b] = U Out[p], where p is a predecessor of b, Out[b] = Gen[b] U (In[b]-Kill[b]), Gen[b] = {set of all variables assigned to in b}, and Kill[b] = {set of definitions from before b} - Gen[b]. In this program, definitions generated are also added to kill, but, because of the union for Out[b], it doesn't impact Out[b].

TestA3 includes a funciton call that returns the addition of two numbers. TestB3 looks at a switch statement.

Dataflow on TestA3 outputs:

block entry
In: 
Gen: x#2 y#3 y#4 i#5 
Kill: x#2 y#3 i#5 y#4 
Out: x#2 y#3 i#5 y#4 

block for.cond
In: x#2 y#3 i#5 y#4 
Gen: 
Kill: 
Out: x#2 y#3 i#5 y#4 

block for.body
In: x#2 y#3 i#5 y#4 
Gen: n#6 
Kill: n#6 
Out: n#6 x#2 y#3 i#5 y#4 

block for.cond1
In: n#6 x#2 y#3 i#5 y#4 
Gen: 
Kill: 
Out: n#6 x#2 y#3 i#5 y#4 

block for.body3
In: n#6 x#2 y#3 i#5 y#4 
Gen: 
Kill: 
Out: n#6 x#2 y#3 i#5 y#4 

block if.then
In: n#6 x#2 y#3 i#5 y#4 
Gen: x#7 y#8 
Kill: x#2 y#3 y#4 x#7 y#8 
Out: n#6 i#5 x#7 y#8 

block if.else
In: n#6 x#2 y#3 i#5 y#4 
Gen: 
Kill: 
Out: n#6 x#2 y#3 i#5 y#4 

block if.then7
In: n#6 x#2 y#3 i#5 y#4 
Gen: y#9 
Kill: y#3 y#4 y#9 y#8 
Out: n#6 x#2 i#5 y#9 

block if.else8
In: n#6 x#2 y#3 i#5 y#4 
Gen: x#10 y#11 
Kill: x#2 y#3 y#4 x#7 x#10 y#11 y#9 y#8 
Out: n#6 i#5 x#10 y#11 

block if.end
In: n#6 x#2 y#3 i#5 y#4 x#10 y#11 y#9 
Gen: 
Kill: 
Out: n#6 x#2 y#3 i#5 y#4 x#10 y#11 y#9 

block if.end9
In: n#6 x#2 y#3 i#5 y#4 x#7 x#10 y#11 y#9 y#8 
Gen: x#12 
Kill: x#2 x#12 x#7 x#10 
Out: n#6 y#3 i#5 x#12 y#4 y#11 y#9 y#8 

block for.end
In: n#6 x#2 y#3 i#5 y#4 
Gen: 
Kill: 
Out: n#6 x#2 y#3 i#5 y#4 

block if.then13
In: n#6 x#2 y#3 i#5 y#4 
Gen: x#13 y#14 x#15 
Kill: x#2 y#3 x#15 x#13 y#14 y#4 x#12 x#7 x#10 y#11 y#9 y#8 
Out: n#6 x#15 x#13 y#14 i#5 

block if.end15
In: n#6 x#2 y#3 x#15 i#5 x#13 y#14 y#4 
Gen: 
Kill: 
Out: n#6 x#2 y#3 x#15 i#5 x#13 y#14 y#4 

block for.end18
In: x#2 y#3 i#5 y#4 
Gen: 
Kill: 
Out: x#2 y#3 i#5 y#4 

This is validated when analyzing the cfg for A3, including the caveat analysis isn't done within a block.

Likewise, Dataflow for TestB3 outputs:

block entry
In: 
Gen: a#2 b#3 c#4 x#5 y#6 z#7 i#8 
Kill: a#2 b#3 c#4 x#5 y#6 z#7 i#8 
Out: a#2 b#3 c#4 x#5 y#6 z#7 i#8 

block for.cond
In: a#2 b#3 c#4 x#5 y#6 z#7 i#8 
Gen: 
Kill: 
Out: a#2 b#3 c#4 x#5 y#6 z#7 i#8 

block for.body
In: a#2 b#3 c#4 x#5 y#6 z#7 i#8 
Gen: z#9 
Kill: z#9 z#7 
Out: z#9 a#2 b#3 c#4 x#5 y#6 i#8 

block sw.bb
In: a#2 z#9 b#3 c#4 x#5 y#6 z#7 i#8 
Gen: j#10 
Kill: j#10 
Out: a#2 z#9 b#3 c#4 j#10 x#5 y#6 z#7 i#8 

block for.cond2
In: a#2 z#9 b#3 c#4 j#10 x#5 y#6 z#7 i#8 
Gen: 
Kill: 
Out: a#2 z#9 b#3 c#4 j#10 x#5 y#6 z#7 i#8 

block for.body4
In: a#2 z#9 b#3 c#4 j#10 x#5 y#6 z#7 i#8 
Gen: 
Kill: 
Out: a#2 z#9 b#3 c#4 j#10 x#5 y#6 z#7 i#8 

block if.then
In: a#2 z#9 b#3 c#4 j#10 x#5 y#6 z#7 i#8 
Gen: x#11 x#12 y#13 
Kill: x#5 x#12 y#13 x#11 y#6 
Out: a#2 z#9 b#3 c#4 j#10 x#12 y#13 x#11 z#7 i#8 

block if.end
In: j#10 x#5 y#6 z#7 a#2 z#9 b#3 c#4 x#12 y#13 x#11 i#8 
Gen: z#14 
Kill: z#9 z#14 z#7 
Out: a#2 b#3 c#4 j#10 z#14 x#5 x#12 y#13 x#11 y#6 i#8 

block for.end
In: a#2 z#9 b#3 c#4 j#10 x#5 y#6 z#7 i#8 
Gen: 
Kill: 
Out: a#2 z#9 b#3 c#4 j#10 x#5 y#6 z#7 i#8 

block sw.bb6
In: a#2 z#9 b#3 c#4 x#5 y#6 z#7 i#8 
Gen: x#15 x#16 
Kill: x#16 x#15 x#5 x#12 x#11 
Out: a#2 z#9 b#3 c#4 x#16 x#15 y#6 z#7 i#8 

block sw.bb7
In: a#2 z#9 b#3 c#4 x#5 y#6 z#7 i#8 
Gen: 
Kill: 
Out: a#2 z#9 b#3 c#4 x#5 y#6 z#7 i#8 

block if.then11
In: a#2 z#9 b#3 c#4 x#5 y#6 z#7 i#8 
Gen: x#17 
Kill: x#17 x#16 x#15 x#5 x#12 x#11 
Out: a#2 z#9 b#3 c#4 x#17 y#6 z#7 i#8 

block if.else
In: a#2 z#9 b#3 c#4 x#5 y#6 z#7 i#8 
Gen: 
Kill: 
Out: a#2 z#9 b#3 c#4 x#5 y#6 z#7 i#8 

block if.then15
In: a#2 z#9 b#3 c#4 x#5 y#6 z#7 i#8 
Gen: z#18 
Kill: z#9 z#18 z#14 z#7 
Out: a#2 b#3 c#4 z#18 x#5 y#6 i#8 

block if.else16
In: a#2 z#9 b#3 c#4 x#5 y#6 z#7 i#8 
Gen: y#19 
Kill: y#19 y#13 y#6 
Out: a#2 z#9 b#3 y#19 c#4 x#5 z#7 i#8 

block if.end17
In: a#2 z#9 b#3 c#4 y#19 z#18 x#5 y#6 z#7 i#8 
Gen: 
Kill: 
Out: a#2 z#9 b#3 c#4 y#19 z#18 x#5 y#6 z#7 i#8 

block if.end18
In: a#2 z#9 b#3 c#4 y#19 x#17 z#18 x#5 y#6 z#7 i#8 
Gen: 
Kill: 
Out: a#2 z#9 b#3 c#4 y#19 x#17 z#18 x#5 y#6 z#7 i#8 

block sw.epilog
In: y#19 x#17 x#16 z#18 j#10 x#5 y#6 z#7 a#2 z#9 b#3 c#4 x#15 i#8 
Gen: x#20 x#21 y#22 
Kill: y#19 x#17 x#16 x#15 x#5 x#12 y#13 x#11 y#6 x#21 y#22 x#20 
Out: a#2 z#9 b#3 c#4 z#18 j#10 x#21 y#22 x#20 z#7 i#8 

block for.end23
In: a#2 b#3 c#4 x#5 y#6 z#7 i#8 
Gen: 
Kill: 
Out: a#2 b#3 c#4 x#5 y#6 z#7 i#8 
