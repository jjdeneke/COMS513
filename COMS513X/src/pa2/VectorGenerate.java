import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.EdgeFactory;
import java.util.HashMap;
import java.io.FileReader;
import java.util.Scanner;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.lang.Integer;
import java.util.Set;
import java.util.Iterator;
import java.util.Map;
import java.io.IOException;


/** Vector Generate class
  * Reads a .dot file and generates vector
  * @author Jonathan Deneke
  */
class VectorGenerate
{
  public static void main(String[] args)
  {
    class VectorFactory implements EdgeFactory<Integer, String>
    {
      public String createEdge(Integer sourceVertex, Integer targetVertex)
      {
        String edge = sourceVertex.toString() + '-' + targetVertex.toString();
        return edge;
      }
    }
    
    //Variables
    String fileName;
    DefaultDirectedGraph<Integer, String> digraph = new DefaultDirectedGraph<Integer, String>(new VectorFactory());
    HashMap<String, Integer> dotLabels = new HashMap<String, Integer>();
    ArrayList<String> labels = new ArrayList<String>();
    Scanner input = new Scanner(System.in);
    int vertexSize = 0;
    HashMap<String, Integer> vectorMap = new HashMap<String, Integer>();
    String vector = "(";

    try
    {
      System.out.println("Enter DOT file: ");
      fileName = input.nextLine();
      FileReader fileVertex = new FileReader(fileName);
      Scanner scanVertex = new Scanner(fileVertex);

      //Read vertexes
      while(scanVertex.hasNext())
      {
        String line = scanVertex.nextLine();

        if(line.contains("shape"))
        {
          int nodeStart = line.indexOf('N');
          int nodeEnd = line.indexOf('[') - 1;
          String node = line.substring(nodeStart, nodeEnd);
          int labelStart = line.indexOf('{') + 1;
          int labelEnd = line.indexOf(':');
          String label = line.substring(labelStart, labelEnd);

          digraph.addVertex(vertexSize);
          dotLabels.put(node, new Integer(vertexSize));
          labels.add(label);
          
          vertexSize++;
        }
      }

      //Read edges
      scanVertex.close();
      fileVertex.close();
      
      FileReader fileEdge = new FileReader(fileName);
      Scanner scanEdge = new Scanner(fileEdge);
      
      while(scanEdge.hasNext())
      {
        String line = scanEdge.nextLine();

        if(line.contains("->"))
        {
          int tailNodeStart = line.indexOf('N');
          int tailNodeEnd;
          if(line.contains(":")) 
          {
            tailNodeEnd = line.indexOf(':');
          } else
          {
            tailNodeEnd = line.indexOf('-') - 1;
          }
          String tailNode = line.substring(tailNodeStart, tailNodeEnd);
          int headNodeStart = line.lastIndexOf('N');
          int headNodeEnd = line.indexOf(';');
          String headNode = line.substring(headNodeStart, headNodeEnd);
          Integer tailNodeLabel = new Integer(dotLabels.get(tailNode));
          Integer headNodeLabel = new Integer(dotLabels.get(headNode));
          String edge = tailNodeLabel.toString() + '-' + headNodeLabel.toString();

          digraph.addEdge(tailNodeLabel, headNodeLabel, edge);
        }
      }
      scanEdge.close();
      fileEdge.close();

      //Create table and vectors

      //n-paths (1-3)
      System.out.println("1-path");
      for(int i = 0; i < vertexSize; i++)
      {
        String node = labels.get(i);

        if(vectorMap.containsKey(node))
        {
          Integer value = vectorMap.get(node);
          vectorMap.put(node, Integer.valueOf(value.intValue() + 1));
        } else
        {
          vectorMap.put(node, Integer.valueOf(1));
        }
        System.out.print(node + ' ');

      }
      System.out.print('\n');
      System.out.println();

      System.out.println("2-path");
      for(int i = 0; i < vertexSize; i++)
      {
        String tail = labels.get(i);
        Set<String> edges = digraph.outgoingEdgesOf(Integer.valueOf(i));
        Iterator<String> edgeIter = edges.iterator();
        while(edgeIter.hasNext())
        {
          String edge = edgeIter.next();
          Integer headNode = new Integer(edge.substring(edge.indexOf('-')+1));
          String head = labels.get(headNode.intValue());
          String output = tail + '-' + head;

          if(vectorMap.containsKey(output))
          {
           Integer value = vectorMap.get(output);
           vectorMap.put(output, Integer.valueOf(value.intValue() + 1));
          } else
          {
           vectorMap.put(output, Integer.valueOf(1));
          }
          System.out.print(output+' ');
        }
      } 
      System.out.print('\n');
      System.out.println();

      System.out.println("3-path");
      for(int i = 0; i < vertexSize; i++)
      {
        String tail = labels.get(i);
        Set<String> firstEdges = digraph.outgoingEdgesOf(i);
        Iterator<String> firstEdgeIter = firstEdges.iterator();
        while(firstEdgeIter.hasNext())
        {
          String firstEdge = firstEdgeIter.next();
          Integer midNode = new Integer(firstEdge.substring(firstEdge.indexOf('-')+1));
          String mid = labels.get(midNode.intValue());
          Set<String> secondEdges = digraph.outgoingEdgesOf(midNode);
          Iterator<String> secondEdgeIter = secondEdges.iterator();
          while(secondEdgeIter.hasNext())
          {
            String secondEdge = secondEdgeIter.next();
            Integer headNode = new Integer(secondEdge.substring(secondEdge.indexOf('-')+1));
            String head = labels.get(headNode.intValue());
            String output = tail + '-' + mid + '-' + head;

            if(vectorMap.containsKey(output))
            {
              Integer value = vectorMap.get(output);
              vectorMap.put(output, Integer.valueOf(value.intValue() + 1));
            } else
            {
              vectorMap.put(output, Integer.valueOf(1));
            }
            System.out.print(output+' ');
          }
        }
      }
      System.out.print('\n');
      System.out.println();

      //(p,q)-nodes
      System.out.println("(p,q)-nodes");
      for(int i = 0; i < vertexSize; i++)
      {
        Integer node = Integer.valueOf(i);
        String label = labels.get(i);
        int p = digraph.inDegreeOf(node);
        int q = digraph.outDegreeOf(node);
        String output = label+'-'+p+'-'+q;
        if(vectorMap.containsKey(output))
        {
          Integer value = vectorMap.get(output);
          vectorMap.put(output, Integer.valueOf(value.intValue() + 1));
        } else
        {
          vectorMap.put(output, Integer.valueOf(1));
        }
        System.out.print(output+' ');
      }
      System.out.print('\n');
      System.out.println();
      

      //Output vector
      System.out.println("Vector");
      Set<Map.Entry<String, Integer>> vectorSet = vectorMap.entrySet();
      Iterator<Map.Entry<String, Integer>> vectorIter = vectorSet.iterator();

      while(vectorIter.hasNext())
      {
        vector = vector + vectorIter.next().getValue() + ", ";
      }
      vector = vector.trim() + ')';

      System.out.println(vector);

    }catch(FileNotFoundException e)
    {
      System.out.println("File Not Found");
    }catch(IOException e)
    {
      System.out.println("Closing Error");
    }
  }
}
